//Logg inn
var login = "<div class='loginItems'>";
login+="<form name='myForm' id='frm1' action class='form row'>";
login+="<div class='form-group col-12'>";
login+="<h4>Logg inn</h4>";
login+="<input type='email username' class='form-control' id='inputEmail' aria-describedby='emailHelp' placeholder='E-post eller brukernavn'>";
login+="</div>";
login+="<div class='form-group col-12'>";
login+="<input type='password' class='form-control' id='inputPassword' placeholder='Passord'>";
login+="</div>";
login+="<div class='col-12 '>";
login+="<a href='#warningBtn'><button id='loginBtn' type='button' class='col-4 btn btn-outline-primary'>Logg inn</button></a>";  
login+="<a class='forgottenRight col-7 btn btn-outline-secondary' href='../html/forgotten.html' role='button'>Glemt passord?</a>";
login+="<a class='regBottom col-12 btn btn-outline-success' href='../html/registration.html' role='button'>Registrer deg</a>";
login+="<div>";
login+="</form>";
login+="</div>";
$('.loginBody').append(login);

$("#loginBtn").click(function(){
    var username = $("#inputEmail").val();
    var password = $("#inputPassword").val();
    localStorage.setItem("user", username);
    if(username=='' && password==''){
        
        $('.loginBody .loginAlert').remove();
        var loginAlert = "<div id='warningBtn' class='col-12 alertBtn loginAlert alert alert-danger' role='alert'>";
            loginAlert+="Fyll ut begge feltene!";
            loginAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            loginAlert+="</div>";
        $('.loginBody').append(loginAlert);
    
    }else if(username=='' && password!==''){
        $('.loginBody .loginAlert').remove();
        var loginAlert = "<div id='warningBtn' class='col-12 alertBtn loginAlert alert alert-danger' role='alert'>";
            loginAlert+="Brukernavn må fylles ut!";
            loginAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            loginAlert+="</div>";
        $('.loginBody').append(loginAlert);
    
    }else if(password=='' && username!==''){
        $('.loginBody .loginAlert').remove();
        var loginAlert = "<div id='warningBtn' class='col-12 alertBtn loginAlert alert alert-danger' role='alert'>";
            loginAlert+="Passord må fylles ut!";
            loginAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            loginAlert+="</div>";
        $('.loginBody').append(loginAlert);
    
    }else{
        $.get("https://182531-www.web.tornado-node.net/api/get_nonce/?controller=user&method=generate_auth_cookie", function(data){
            getNonce = data.nonce;
            $.get("https://182531-www.web.tornado-node.net/api/user/generate_auth_cookie/?nonce="+getNonce+"&username="+username+"&password="+password, function(data){
                localStorage.setItem("getCookie", data.cookie);
                localStorage.setItem("getBrukerId", data.user.id);
                $.get("https://182531-www.web.tornado-node.net/api/user/validate_auth_cookie/?cookie="+localStorage.getItem("getCookie"), function(data) {
                    localStorage.setItem("getValid", data.valid);
        
                    if(localStorage.getItem("getValid") == "true"){
                        window.location = "../index.html";
                    }else if(localStorage.getItem("getValid") == "false"){
                        $('.loginBody .loginAlert').remove();
        var loginAlert = "<div class='col-12 loginAlert alert alert-danger' role='alert'>";
            loginAlert+="Brukernavn eller passord er feil!";
            loginAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            loginAlert+="<span aria-hidden='true'>&times;</span>";
            loginAlert+="</div>";
        $('.loginBody').append(loginAlert);
                        $("#inputEmail").val("");
                        $("#inputPassword").val("");
                        localStorage.removeItem("getValid");
                    }
                })
            })
        })
    }      
})