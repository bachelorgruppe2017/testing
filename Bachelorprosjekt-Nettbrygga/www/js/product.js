//Product
var product_slug = getParameterByName('kategori');

jQuery.get(url + "wp-json/wc/v1/products/?filter[product]="+product_slug+"&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
          
    $.each(data,function(rowindex){
       
        var row = data[rowindex];
        var product = "<div class='col productItems'>";
        product+="<img src='"+row.images[0].src+"' + class='col-12'>";
        product+="<div class='products col-12'>";
        product+="<h2 class=''>"+row.name+"</h2>";
        product+="<p>"+row.short_description+"</p>";
        product+="</div>";
        product+="<h5>"+row.price_html+"</h5>";
        product+="<p>"+row.stock_quantity+" på lager"+"</p>";
        product+="<a href='#warningBtn'><button class='col-12 btn btn-outline-success' id='shopBtn'>"+row.button_text+"LEGG I HANDLEKURVEN"+"</button></a>";
        product+="</div>";
        $('.productBody').append(product);
        
        $("#shopBtn").click(function(){
            var getOrder = localStorage.getItem("getOrderId");
            
            if(row.stock_quantity == null){
                $('.productBody .alert').remove();
                var productAlert = "<div id='warningBtn' class='col-12 alertBtn productAlert alert alert-danger' role='alert'>";
                productAlert+="Ingen fler på lager";
                productAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
                productAlert+="</div>";
                $('.productBody').append(productAlert);
                   
            }else if(getOrder === null){
                $.post(url + "wp-json/wc/v2/orders?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,{  
                    line_items: [{
                        product_id: row.id,
                        quantity: 1
                    },
                    ],
                },
                       
                function(response,status){ 
                    $.get(url + "wp-json/wc/v2/orders?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
                         
                        localStorage.setItem("getOrderId", data[0].id);
                        window.location.reload();
                    })
                });

            }else if(getOrder === 'null'){
                $.post(url + "wp-json/wc/v2/orders?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,{  
                    line_items: [{
                        product_id: row.id,
                        quantity: 1
                    },
                    ],
                },
                       
                function(response,status){  // Required Callback Function
                    $.get(url + "wp-json/wc/v2/orders?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
                        
                        localStorage.setItem("getOrderId", data[0].id);
                       window.location.reload();
                    })
                });

            }else if(getOrder !== null || 'null' || ""){
                $.post(url + "wp-json/wc/v2/orders/"+localStorage.getItem("getOrderId")+"?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,{ 
                    line_items: [{
                        product_id: row.id,
                        quantity: 1
                    },
                    ],
                }, 
                       
                function(response,status){ 
                    
                    window.location.reload();
                   
                });
            }
        });
    });
});

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, "\\$&");
   var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, " "));
}