//Shopping
var item = "<div class='col-12 shoppingEmpty'>";
item+="<h4 class='handlekurv'>Handlevogn</h4>";
item+="<p>Din vogn er tom.</p>";
item+="<a href='../index.html'>";
item+="<button id='myBtn' type='button' value='Next' class='btn btn-outline-secondary'>Tilbake til butikken";
item+="</button>";
item+="</a>";
item+="</div>";
$('.shoppingBodyen').append(item);

var getOrder = localStorage.getItem("getOrderId");
if(getOrder !== ""){
    $.get(url + "wp-json/wc/v1/orders/"+getOrder+"?&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
    
        $('.shoppingEmpty').remove();
        var item = "<div class='col-12 shopping '>";
        item+="<table class='table col'>";
        item+="<thead>";
        item+="<tr>";
        item+="<th class=''>Handlevogn</th>"; 
        item+="<th class=''>Antall</th>";
        item+="<th class=''>Pris</th>";
        item+="</tr>";
        item+="</thead>";
        item+="</table>";
        item+="</div>";
        $('.shoppingHeader').append(item);   
    
        $.each(data.line_items,function(rowindex){
            var row = data.line_items[rowindex];
        console.log(data.line_items.length);
            $.get(url + "wp-json/wc/v1/products/"+row.product_id+"?&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){

                var item = "<div class='col-12 row shoppingItems'>";
                item+="<img class='col-3' src='"+data.images[0].src+"'></img>";
                item+="<p class='col'>"+row.name+"</p>";
                item+="<p class='col'>"+row.quantity+""+"</p>";
                item+="<p class='col'>"+row.price+"</p>";
                item+="</div>";
                $('.shoppingBodyen').append(item);
                
                $('.delete').click(function(){             
                    $.get(url + "wp-json/wc/v2/orders/"+getOrder+"?_method=DELETE&force=true&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,

                    function(response,status){
                        window.location = "shopping.html";
                        localStorage.setItem("getOrderId", null);
                    });
                }) 
        
                $('.alert').click(function(){
                    $('.shoppingFooter .shoppingAlert').remove();
                    
                    var item = "<div class='col-12'>";
                    item+="<div class='shoppingAlert alert alert-danger' role='alert'>";
                    item+="Sikker på at du vil slette hele handlekurven?";
                    item+="<button id='warningBtn' type='button' class='close delete' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
                    item+="</div>";
                    item+="</div>";
        $('.shoppingFooter').append(item);
                    
                    $('.delete').click(function(){     
                    $.get(url + "wp-json/wc/v2/orders/"+getOrder+"?_method=DELETE&force=true&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,{  
                        line_items: [{
                            id: row.id,
                        },
                        ],
                    },

                    function(response,status){
                        window.location = "shopping.html";
                        localStorage.setItem("getOrderId", null);
                    });
                }) 
                }) 
            });
        });
    
        var item = "<div class='col-12 shoppingTotal'>";
        item+="<table class='table'>";
        item+="<thead>";
        item+="<tr>";
        item+="<th class='col'><h6>" + "Total sum varer: "+data.total+"</h6></th>";
        item+="<th><a href='#warningBtn'><button type='button' class='alert close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></a></th>";
        item+="</tr>";
        item+="</thead>";
        item+="</table>";
        item += "<td><button class='col-12 btn btn-outline-success' id='checkoutBtn'>" + "GÅ TIL BETALING" + "</button></td>";
        item+="</div>";
        $('.shoppingFooter').append(item);  
           
        var handler = StripeCheckout.configure({
                key: 'pk_test_3MoGRoTPUU6v2BAy2qwARIlF',
                locale: 'auto',
                token: 
                function(token) {
                    $.ajax({
                        type: 'POST',
                        url: 'https://api.stripe.com/v1/charges',
                        headers: {
                            Authorization: 'Bearer sk_test_OXjlxfOM44WzcY0V1Jw1rQAX'
                        },
                        data: {
                            amount: parseInt(data.total * 100),
                            currency: 'nok',
                            source: token.id
                        },

                        success: (response) => {
                            localStorage.setItem("paid", "");
                            console.log('successful payment: ', response);
                            localStorage.setItem("paid", response.captured)
                        },
                        error: (response) => {
                            console.log('error payment: ', response);
                        },

                    })
                }
            });

        $("#checkoutBtn").click(function(e) {
            handler.open({
                name: 'STRIPE BETALING',
                currency: 'nok',
                amount: parseInt(data.total * 100)
            });

            window.addEventListener('popstate', function() {
                handler.close({
                });
            });

            if (localStorage.getItem("getValid") == "true") {
                $.get(url + "api/user/get_user_meta/?cookie=" + localStorage.getItem("getCookie") + "&user_id=" + localStorage.getItem("user"), function(data) {
                    var fname = data.billing_first_name;
                    var lname = data.billing_last_name;
                    var address = data.billing_address_1;
                    var city = data.billing_city;
                    var postcode = data.billing_postcode;
                    var email = data.billing_email;

                $.post(url + "wp-json/wc/v2/orders/" + localStorage.getItem("getOrderId") + "?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, {
                        customer_id: localStorage.getItem("getBrukerId"),
                        billing: {
                            first_name: fname,
                            last_name: lname,
                            address_1: address,
                            city: city,
                            postcode: postcode,
                            email: email
                        },
                        shipping: {
                            first_name: fname,
                            last_name: lname,
                            address_1: address,
                            city: city,
                            postcode: postcode,
                            email: email
                        },
                    },
                    function(response, status) {
                        if (localStorage.getItem("paid") == "true") {
                            $.post(url + "wp-json/wc/v2/orders/" + localStorage.getItem("getOrderId") + "?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, {
                                    status: "on-hold"
                            },
                                function(response, status) {})
                        }
                    });
                });
            } else {
                window.location = "login.html";
            }
        });
    });
}