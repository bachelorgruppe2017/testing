//Registrering
$(function() {
    
    var reg = "<div class='regItems'>";
    reg+="<form name='myForm' id='frm1' action class='form row'>";
    reg+="<div class='form-group col-12'>";
    reg+="<h4>Registrering</h4>";
    reg+="<input type='email' class='form-control' id='inputEmail' aria-describedby='emailHelp' placeholder='Email'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<input type='password' class='form-control' id='inputPassword' placeholder='Passord'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<input type='firstname' class='form-control' id='inputFirstname' placeholder='Fornavn'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<input type='lastname' class='form-control' id='inputLastname' placeholder='Etternavn'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<input type='phone' class='form-control' id='inputPhone' placeholder='Telefon'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<input type='address' class='form-control' id='inputAddress' placeholder='Adresse'>";
    reg+="</div>";
    reg+="<div class='form-group col-12'>";
    reg+="<select class='form-control' id='inputCountry'>";
    reg+="<option>Land</option>";
    reg+="<option>Norge</option>";
    reg+="<option>Sverige</option>";
    reg+="<option>Danmark</option>";
    reg+="</select>";
    reg+="</div>";
    reg+="<div class='form-group col-5'>";
    reg+="<input type='postcode' class='form-control' id='inputPostcode' placeholder='Postnr.'>";
    reg+="</div>";
    reg+="<div class='form-group col-7'>";
    reg+="<input type='city' class='form-control' id='inputCity' placeholder='By'>";
    reg+="</div>";
    reg+="<div class='form-group col'>";
    reg+="<a href='#warningBtn'><button id='regBtn' type='button' value='Next' class='col-4 btn btn-outline-primary'>Fullfør";
    reg+="</button></a>";
    reg+="</div>";
    reg+="</form>";
    reg+="</div>";
    $('.regBody').append(reg);
});

$(document).ready(function(){
    $("#regBtn").click(function(){
        
        var vemail = $("#inputEmail").val();
        var vpassword = $("#inputPassword").val();
        var vfname = $("#inputFirstname").val();
        var vlname = $("#inputLastname").val();
        var vaddress = $("#inputAddress").val();
        var vpostcode = $("#inputPostcode").val();
        var vcity = $("#inputCity").val();
        var vcountry = $("#inputCountry").val();
        var vphone = $("#inputPhone").val();
        
        if(vemail=='' || vpassword=='' || vfname=='' || vlname=='' || vaddress=='' || vpostcode=='' || vcity=='' || vcountry=='' || vphone==''){       
            $('.regBody .alert').remove();
            var regAlert = "<div id='warningBtn' class='col-12 alertBtn alert alert-danger' role='alert'>";
            regAlert+="Fyll ut alle feltene!";
            regAlert+="<button type='button' class='close' data-dismiss='alert' aria-label='Close'>";
            regAlert+="</div>";
            $('.regBody').append(regAlert);
        
        }else{         
            $.post(url + "wp-json/wc/v2/customers?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret,{     
                email:vemail,
                password:vpassword,
                first_name:vfname,
                last_name:vlname,
                billing: {
                    first_name:vfname,
                    last_name:vlname,
                    address_1:vaddress,
                    postcode:vpostcode,
                    email:vemail,
                    city:vcity,
                    country:vcountry,
                    phone:vphone
                },
                shipping: {
                    first_name:vfname,
                    last_name:vlname,
                    address_1:vaddress,
                    postcode:vpostcode,
                    city:vcity,
                    country:vcountry
                }
            },
               
            function(response,status){  // Required Callback Function
                $("#form")[0].reset();
            });
            window.location = "login.html";
        }
    });
});