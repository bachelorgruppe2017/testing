//Article
var product_slug = getParameterByName('kategori');

jQuery.get(url + "wp-json/wc/v1/products/?filter[product_cat]="+product_slug+"&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
          
    $.each(data,function(rowindex){
    
        var row = data[rowindex];
        var article = "<div class='col-6 articleItems'>";
        article+="<a class='articleText' href ='../html/product.html?kategori="+row.slug+"'";
        article+="<h1>"+"</h1>";
        article+="<img src='"+row.images[0].src+"'>";
        article+="<h5>"+row.name+"</h5>";
        article+="<p>"+row.price_html+"</p>";
        article+="</a>";
        article+="</div>";
        $('.articleBody').append(article);
    });
});

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, "\\$&");
   var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
       results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, " "));
}