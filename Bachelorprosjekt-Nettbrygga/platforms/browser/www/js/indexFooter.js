//Footer - Kode for å oppdatere handlekurv med tall 
$.get(url + "wp-json/wc/v1/orders/"+localStorage.getItem("getOrderId")+"?&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
        
    var footer = "<div class='footerUpdated bg-dark'>";
    footer+="<div class='row'>";
    footer+="<div id='collapseTwo' class='collapse col-12'>";
    footer+="<input id='inputSearch' class='search form-control' type='search' placeholder='Søk'>";
    footer+="<a class='collapsed' data-toggle='collapse' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'>";
    footer+="<button id='searchBtn' class='col-3 searching btn btn-outline-success my-2 my-sm-0' type='submit'>Søk</button>";
    footer+="</a>";
    footer+="</div>";
    
    if(localStorage.getItem("getValid") == "true"){
        footer+="<a href='html/profil.html' class='col-4'>";
        footer+="<i class='image fa fa-user fa-lg' aria-hidden='true'></i>";
        footer+="</a>";
    }else if(localStorage.getItem("getValid") !== "true"){
        footer+="<a href='html/login.html' class='col-4'>";
        footer+="<i class='image fa fa-user fa-lg' aria-hidden='true'></i>";
        footer+="</a>";
    }
    
    footer+="<h5 class='mb-0 col-4'>";
    footer+="<a class='collapsed' data-toggle='collapse' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'>";
    footer+="<i class='image fa fa-search fa-lg' aria-hidden='true'></i>";
    footer+="</a>";
    footer+="</h5>";
    footer+="<a href='html/shopping.html' class='col-4'>";
    footer+="<i class='image fa fa-shopping-cart fa-lg' aria-hidden='true'></i>";
    footer+="<span class='badge badge-dark'><p>"+data.line_items.length+"</p></span>";
    footer+="</a>";
    footer+="</div>";
    footer+="</div>";
    $('.indexFooter').append(footer);
});
    
//Footer - Kode med 0 som standard tall 
var footer = "<div class='footer bg-dark'>";
footer+="<div class='row'>";
footer+="<div id='collapseTwo' class='collapse col-12'>";
footer+="<input id='inputSearch' class='search form-control' type='search' placeholder='Søk'>";
footer+="<a class='collapsed' data-toggle='collapse' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'>";
footer+="<button id='searchBtn' class='col-3 searching btn btn-outline-success my-2 my-sm-0' type='submit'>Søk</button>";
footer+="</a>";
footer+="</div>";

if(localStorage.getItem("getValid") == "true"){
    footer+="<a href='html/profil.html' class='col-4'>";
    footer+="<i class='image fa fa-user fa-lg' aria-hidden='true'></i>";
    footer+="</a>";
} else if(localStorage.getItem("getValid") !== "true"){
    footer+="<a href='html/login.html' class='col-4'>";
    footer+="<i class='image fa fa-user fa-lg' aria-hidden='true'></i>";
    footer+="</a>";
}

footer+="<h5 class='mb-0 col-4'>";
footer+="<a class='collapsed' data-toggle='collapse' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'>";
footer+="<i class='image fa fa-search fa-lg' aria-hidden='true'></i>";
footer+="</a>";
footer+="</h5>";
footer+="<a href='html/shopping.html' class='col-4'>";
footer+="<i class='image fa fa-shopping-cart fa-lg' aria-hidden='true'></i>";
footer+="<span class='badge badge-dark'>0</span>";
footer+="</a>";
footer+="</div>";
footer+="</div>";
$('.indexFooter').append(footer);

//Søke funksjon
$("#searchBtn").click(function(){
        
    var søk = $("#inputSearch").val();
    if(søk!==''){     
            
        $('.indexItems, .articleItems, .productItems, .contactItems, loginItems, regItems, forgottenItems, profilItems, shoppingItems').remove();
        $.get(url + "wp-json/wc/v1/products/?search="+søk+"&consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
           
            $.each(data,function(rowindex){
                           
                var row = data[rowindex];
                var item = "<div class='col-6 articleItems'>";
                item+="<a class='articleText' href ='html/product.html?kategori="+row.slug+"'";
                item+="<h1>"+"</h1>";
                item+="<img src='"+row.images[0].src+"'>";
                item+="<h5>"+row.name+"</h5>";
                item+="<p>"+row.price_html+"</p>";
                item+="</div>";
                $('.indexBody').append(item);
                $("#inputSearch").val("")
            });  
        });
    }else if(søk==''){
        
        $('.indexItems, .articleItems, .productItems, .contactItems, loginItems, regItems, forgottenItems, profilItems, shoppingItems').remove();
        jQuery.get(url + "wp-json/wc/v1/products/categories?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){
          
            $.each(data,function(rowindex){
       
                var row = data[rowindex];
                var item = "<div class='col-6 indexItems'>";
                item+="<a class='indexText' href ='./html/article.html?kategori="+row.slug+"'";
                item+="<h2>"+"</h2>";
                item+="<img src='"+row.image.src+"'>";  
                item+="<h4>"+row.name+"</h4>";
                item+="</div>";
                item+="</a>";
                $('.indexBody').append(item);
            });
        });      
    }
});