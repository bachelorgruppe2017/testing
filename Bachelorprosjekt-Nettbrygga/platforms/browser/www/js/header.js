//Header               
jQuery.get(url + "wp-json/?consumer_key=" + consumerkey + "&consumer_secret=" + consumerSecret, function(data){

    var header = "<div class='header'>";
    header+="<div class='navbar bg-dark'>";
    header+="<a class='title' href='../index.html'>" + data.name + "</a>";
    header+="<button class='burger navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNavAltMarkup' aria-controls='navbarNavAltMarkup' aria-expanded='false' aria-label='Toggle navigation'>";
    header+="<i class='image fa fa-bars fa-lg' aria-hidden='true'></i>";
    header+="</button>";
    header+="<div class='collapse navbar-collapse' id='navbarNavAltMarkup'>";
    header+="<div class='navbar-nav'>";
    header+="<a class='nav-item menu' href='contact.html'>Kontakt</a>";
    if(localStorage.getItem("getValid") == "true"){
        header+="<a class='nav-item menu' id='logout' href='../index.html'>Logg ut</a>";
    } else if(localStorage.getItem("getValid") !== "true"){
        header+="<a class='nav-item menu' href='login.html'>Logg inn</a>";
    }
    header+="</div>";
    $('.header').append(header);
    $("#logout").click(function(){
        localStorage.setItem("getValid", false);
        localStorage.setItem("getCookie", "");
    })
});