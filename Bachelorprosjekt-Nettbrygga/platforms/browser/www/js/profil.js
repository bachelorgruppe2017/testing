//Profil
jQuery.get(url + "api/user/get_user_meta/?cookie=" + localStorage.getItem("getCookie") +"&user_id=" + localStorage.getItem("user"), function(data){
    
    var profil = "<div class='col profilItems'>";
    
        profil+="<h4>Min oversikt</h4>";
        profil+="<div class='card user'>";
        profil+="<div class='card-header'>";
        profil+="Brukerinformasjon";
        profil+="</div>";
        profil+="<div class='card-body'>";
        profil+="<h4 class='card-title'>" + data.first_name + " " + data.last_name + "</h4>";
        profil+="<p class='card-text'>" + data.billing_email + "</p>";
        profil+="<a href='#' class='btn btn-outline-secondary'>Endre</a>";
        profil+="</div>";
        profil+="</div>";
        profil+="<div class='card adress'>";
        profil+="<div class='card-header'>";
        profil+="Adresseinformasjon";
        profil+="</div>";
        profil+="<div class='card-body'>";
        profil+="<h4 class='card-title'>" + data.billing_address_1 + "</h4>";
        profil+="<p class='card-text'>" + data.billing_city + " " + data.billing_postcode + "</p>";
        profil+="<a href='#' class='btn btn-outline-secondary'>Endre</a>";
        profil+="</div>";
        profil+="</div>";
        profil+="</div>";
    $('.profilBody').append(profil);
});